# NDD Docker pgAdmin 4

## Version 0.3.1

- Transfer to GitLab

## Version 0.3.0

- Update to version 1.6

## Version 0.2.0

- Update to version 1.5

## Version 0.1.1

- Add a reference to `ddidier/pgadmin3`

## Version 0.1.0

- Initial commit with version 1.4
