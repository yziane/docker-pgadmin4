# NDD Docker pgAdmin 4

<!-- MarkdownTOC -->

1. [Introduction](#introduction)
1. [Installation](#installation)
    1. [From source](#from-source)
    1. [From Docker Hub](#from-docker-hub)
1. [Usage](#usage)
1. [TODO](#todo)

<!-- /MarkdownTOC -->



<a id="introduction"></a>
## Introduction

A Docker image for [pgAdmin 4](https://www.pgadmin.org/).

References:

- image on Docker Hub : https://hub.docker.com/r/ddidier/pgadmin4
- sources on GitLab : https://gitlab.com/ddidier/docker-pgadmin4

A Docker image for [pgAdmin3 is available too](https://hub.docker.com/r/ddidier/pgadmin/)!

The image is based upon the official [httpd:2.4](https://hub.docker.com/_/httpd/) image and the official [pgAdmin4 release](https://www.pgadmin.org/download/pip4.php).



<a id="installation"></a>
## Installation

<a id="from-source"></a>
### From source

```
git clone git@gitlab.com:ddidier/docker-pgadmin4.git
cd docker-pgadmin4
docker build -t ddidier/pgadmin4 .
```

<a id="from-docker-hub"></a>
### From Docker Hub

```
docker pull ddidier/pgadmin4
```



<a id="usage"></a>
## Usage

The settings are stored in the container in the directory `/data`. You have multiple choices to keep them:

- never delete your container
- use a data volume e.g. `-v $HOST_DATA_DIR:/data`
- use a data volume container e.g. `--volumes-from pgadmin4-data`

If you store these data in a directory like `~/.docker-data/<CONTAINER_NAME>`, you can run this image using:

```bash
docker run --rm                       \
    -p <HOST_PORT>:80                 \
    -v ~/.docker-data/pgadmin4:/data  \
    --name pgadmin4                   \
    ddidier/pgadmin4
```

The configuration is set the first time the container is started and is **then immutable**.

The default values are:

| Environment variable  | Default value     |
|-----------------------|-------------------|
| DEFAULT_SERVER_HOST   | localhost         |
| DEFAULT_SERVER_PORT   | 5050              |
| SQLITE_TIMEOUT        | 1000              |
| MAIL_SERVER           | localhost         |
| MAIL_PORT             | 25                |
| MAIL_USE_SSL          | False             |
| MAIL_USE_TLS          | False             |
| MAIL_USERNAME         |                   |
| MAIL_PASSWORD         |                   |
| MAIL_DEBUG            | False             |
| UPGRADE_CHECK_ENABLED | True              |
| PGADMIN_SETUP_EMAIL   | admin@example.com |
| PGADMIN_SETUP_PASSWORD| changeme          |

You may change those using `-e` parameters when starting the container **for the first time**, or extend the image with someting like this:

```docker
FROM       ddidier/pgadmin4
MAINTAINER David DIDIER

ENV \
    DEFAULT_SERVER_HOST=localhost \
    DEFAULT_SERVER_PORT=5050 \
    SQLITE_TIMEOUT=500 \
    MAIL_SERVER=localhost \
    MAIL_PORT=25 \
    MAIL_USE_SSL=False \
    MAIL_USE_TLS=False \
    MAIL_USERNAME = \
    MAIL_PASSWORD = \
    MAIL_DEBUG=False \
    UPGRADE_CHECK_ENABLED=True \
    PGADMIN_SETUP_EMAIL=admin@example.com \
    PGADMIN_SETUP_PASSWORD=changeme
```



<a id="todo"></a>
## TODO

- [ ] activate HTTPS on Apache `virtualhost`.
- [ ] give a ServerName
- [ ] filter too many logs
